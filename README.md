# Google Sheet Database / Node RED Jobs / Form.IO Timeseries

### Tips
- Use this [LINK](https://www.youtube.com/watch?v=7I2s81TsCnc) to get a Service Account Key

This repo includes a python script that call be called from a host running Node-RED via
a shell execution node. This block takes in an array of spreadsheet cells as strings. The
Node RED scheduler runs every 1 hour and pulls data from a the given cells that are configured
in the change node of the flow. Updates would need to be made downstream as well (This piece could
use some improvement).

There are JSON configs for both NodeRED flow and Form.io for but these can be used as test
examples for any other timeseries operation.

In the function block before the shell execution can configure the path to the python script
on Node RED host.

To query from Google Sheets you'll also need a Service Account and configure the Title of the 
spreadsheet inside the python script. This could be improved as well.


