####################################################
## Youtube video for get auth
## https://www.youtube.com/watch?v=7I2s81TsCnc
####################################################

import sys
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']

credentials = ServiceAccountCredentials.from_json_keyfile_name('service-account-new.json', scope)

gc = gspread.authorize(credentials)

wks = gc.open("Crypto & Stock 2022").sheet1

# print(wks.get_all_records())

val = wks.acell(sys.argv[1]).value.replace('$', '').replace(',', '')
print(float(val))
