####################################################
## Youtube video for get auth
## https://www.youtube.com/watch?v=7I2s81TsCnc
####################################################

import sys
import gspread
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
credentials = ServiceAccountCredentials.from_json_keyfile_name('service-account-new.json', scope)
gc = gspread.authorize(credentials)

## Spreadsheet Title
wks = gc.open("Crypto & Stock 2022").sheet1

def convert_argv_to_array():
  arr = sys.argv[1].split(',')
  return arr

def fetch_all_records():
  return wks.get_all_records()

def fetch_cell(cell):
  cell = wks.acell(cell).value.replace('$', '').replace(',', '')
  return float(cell)

def fetch_multiple_cells(keys):
  cell_values = []
  for key in range(0, len(keys)):
    cell = fetch_cell(keys[key])
    cell_values.append(cell)
  return cell_values

print(fetch_multiple_cells(convert_argv_to_array()))
